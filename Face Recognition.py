import numpy as np
import random
import pandas as pd
import matplotlib.pyplot as plt

# Helper function for showing the original images

def show_orignal_images(pixels):
    # Displaying Orignal Images
    fig, axes = plt.subplots(6, 10, figsize=(11, 7),
                             subplot_kw={'xticks': [], 'yticks': []})
    for i, ax in enumerate(axes.flat):
        ax.imshow(np.array(pixels)[i].reshape(64, 64), cmap='gray')
    plt.show()
    
# Helper function for showing the eigen faces

def show_eigenfaces(pca):
    #Displaying Eigenfaces
	fig, axes = plt.subplots(3, 8, figsize=(9, 4),
	                         subplot_kw={'xticks':[], 'yticks':[]})
	for i, ax in enumerate(axes.flat):
	    ax.imshow(pca[i].reshape(64, 64), cmap='gray')
	    ax.set_title("pca " + str(i+1))
	plt.show()
 
 # Function for splitting the dataset into training and testing

def train_test_split(dataset, train_size=0.6):
    dataset.pop(0)
    testing_set = []
    training_set = []
	# for loop is required to ensure that every person's data
	#  is represented in the training dataset
    for i in range(40):
        person_data = dataset[i*10:(i+1)*10]
        #random.shuffle(person_data)
        training_set += person_data[:int(train_size*10)]
        testing_set += person_data[int(train_size*10):]
    return training_set, testing_set

 # Function for displaying the confusion matrix
 
def plot_confusion_matrix(df_confusion):
    plt.figure(figsize=(20, 20), linewidth=2)
    plt.matshow(df_confusion, fignum=1) # imshow
    plt.colorbar()
    tick_marks = np.arange(40)
    plt.xticks(tick_marks, range(40), rotation=45)
    plt.yticks(tick_marks, range(40))
    ax = plt.gca()
    ax.set_xticks(np.arange(-.5, 40, 1), minor=True)
    ax.set_yticks(np.arange(-.5, 40, 1), minor=True)
    plt.grid(which='minor', color='black', linewidth = 2)

# Opening our dataset
f = open("face_data.csv")

arr = []
for x in f:
    x = list(map(float, x.split(',')))
    arr.append(x)
    
# Converting the array into numpy array

face_data = np.array(arr)

# Dimensions of our dataset
print(face_data.shape)   

# Splitting the dataset into features and labels :
# Feature -> Face Vector
# Label   -> Target

target = face_data[:, [4096]]
dataset = np.delete(face_data, 0, 0)
dataset = np.delete(dataset, 4096, 1)

# Visualizing our original images
show_orignal_images(dataset)

# Splitting the dataset into training and testing
training_set, testing_set = train_test_split(arr, 0.6)

# Converting the arrays into numpy arrays
training_set = np.array(training_set)
testing_set = np.array(testing_set)

# Separating Features and Labels
training_set_target = training_set[:, [4096]]
training_set = np.delete(training_set, 4096, 1)

# Dimension of our training dataset
print(training_set.shape)
testing_set_target = testing_set[:, [4096]]
testing_set = np.delete(testing_set, 4096, 1)

# Visualizing the training dataset
show_orignal_images(training_set)

# Normalisation of each sample by zeroing the mean and the variance 1
face_vector = training_set.transpose()
avg_face_vector = face_vector.mean(axis=1)
avg_face_vector = avg_face_vector.reshape(face_vector.shape[0], 1)
normalized_face_vector = face_vector - avg_face_vector

# Visualizing the normalized face vectors
show_orignal_images(normalized_face_vector.transpose())

#  Computing Covariance Matrix
covariance_matrix = np.transpose(normalized_face_vector).dot(normalized_face_vector)

# Separating out the Eigen Values and Eigen Vectors from covariance matrix
eigen_values, eigen_vectors = np.linalg.eig(covariance_matrix)

# Dimensions of Eigen Vectors
print(eigen_vectors.shape)

# Reducing the dimensions of Eigen Vectors from 240 to just 72
k = 72
k_eigen_vectors = eigen_vectors[0:k, :]
print(k_eigen_vectors.shape)

# Calculating the Eigen Faces
eigen_faces = k_eigen_vectors.dot(np.transpose(normalized_face_vector))

# Dimensions of Eigen Faces
print(eigen_faces.shape)

# Visualizing the Eigen Faces
show_eigenfaces(eigen_faces)

weights = np.transpose(normalized_face_vector).dot(np.transpose(eigen_faces))
print(weights)

# Calculate predicted and actual values for faces to test model
Y_pred = []

for i in range(160):
    test_img = testing_set[i]
    test_img = test_img.reshape(4096, 1)
    test_normalized_face_vector = test_img - avg_face_vector
    test_weight = np.transpose(test_normalized_face_vector).dot(np.transpose(eigen_faces))
    index =  np.argmin(np.linalg.norm(test_weight - weights, axis=1))    
    Y_pred.append(index)

Y_pred = list(map(lambda x: x//6, Y_pred))
Y_act = list(map(lambda x: x//4, range(160)))

# Calculate the accuracy of the model
print(sum(map(lambda x: x[0]==x[1], zip(Y_act, Y_pred)))/160)

# Calculate the confusion matrix based on predicted and actual Y values
confusion_matrix = [[0 for _ in range(40)] for __ in range(40)]
for m, n in zip(Y_act, Y_pred):
    confusion_matrix[m][n] += 1
    
plot_confusion_matrix(np.matrix(confusion_matrix))